This is the initial set of MIBs for Check_MK to allow traps sent from Check_MK to
be recognized by other NMSes.

As Check_MK was initially related to nagios this MIB set is based on https://github.com/monitoring-plugins/nagios-mib.

Feedback should be directed to https://gitlab.com/a3k/cmk-mib
